#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "captcha.h"

#define SAFE_INTERVAL 8 //Constante para heuristica de checagem de manchas

void printMask(PMGImage *img) {

    for(int i = 0; i < img->height; i++) {
        for(int j = 0; j < img->width; j++) {
            printf("%d ", img->content[i][j]);
        }
        puts("");
    }
}

void printPMGImage(PMGImage *img) {
    int bigger = checkLargerDimension(img);

    for(int i = 0; i < bigger; i++) {
        for(int j = 0; j < bigger; j++) {
            printf("%d ", img->content[i][j]);
        }
        puts("");
    }
}

// Função responsável por ler o conteúdo do arquivo e parsear para uma struct do tipo PMGImage
int readImage(char *fileName, PMGImage *img) {
    
    FILE *f = fopen(fileName, "r+");

    fscanf(f, "%c%c", &img->magicNumber[0], &img->magicNumber[1]);
    fgetc(f);
    fscanf(f, "%d", &img->width);
    fgetc(f);
    fscanf(f, "%d", &img->height);
    fgetc(f);
    fscanf(f, "%d", &img->topGrey);
    fgetc(f);

    if( (memoryAllocatePMGImage(img)) ) {
        printf("ALLOCATION ERROR");
        return ALLOCATION_ERROR;
    }

    if( (PGMTypeCheck(img)) ) {
        printf("INVALID PGM TYPE");
        return INVALID_PGM_TYPE;
    }

    //Parseando PMGImg
    char ch_aux;
    int int_aux;
    for(int i = 0; i < img->height; i++) { 
        for(int j = 0; j < img->width; j++) {
           
            fscanf(f, "%d", &int_aux);
            ch_aux = fgetc(f);
            
            // Skipping \n
            if((int)ch_aux == 10)
                continue;

            img->content[i][j] = int_aux;
        }
    }

    // printCaptcha(img);

    return SUCCESS;    
}

int readImageMask(char *fileName, PMGImage *img) {
    // printf("%s", fileName);
    FILE *f_mask = fopen(fileName, "r+");
    if(f_mask == NULL)
        return -1;

    fscanf(f_mask, "%c%c", &img->magicNumber[0], &img->magicNumber[1]);
    fgetc(f_mask);
    fscanf(f_mask, "%d", &img->width);
    fgetc(f_mask);
    fscanf(f_mask, "%d", &img->height);
    fgetc(f_mask);
    fscanf(f_mask, "%d", &img->topGrey);
    fgetc(f_mask);

    if( (memoryAllocatePMGImageAux(img)) ) {
        printf("ALLOCATION ERROR");
        return ALLOCATION_ERROR;
    }

    if( (PGMTypeCheck(img)) ) {
        printf("INVALID PGM TYPE");
        return INVALID_PGM_TYPE;
    }

    //Parseando PMGImg
    char ch_aux;
    int int_aux;
    for(int i = 0; i < img->height; i++) { 
        for(int j = 0; j < img->width; j++) {
           
            fscanf(f_mask, "%d", &int_aux);
            ch_aux = fgetc(f_mask);
            
            // Skipping \n
            if((int)ch_aux == 10)
                continue;

            img->content[i][j] = int_aux;
        }
    }

    // // printCaptcha(img);

    return SUCCESS;    
}

// Inicializa uma PMGImage
void newPMGImage(PMGImage *img) {
    strcpy(img->magicNumber, "");
    img->height = 0;
    img->width = 0;
    img->topGrey = 0;
    img->content = NULL;
}

// Inicializa a estrutura que possui um contador para cada mascara
void newMASKCounter(MASKCounters *mc) {
    mc->size = 9;    
    mc->counters = calloc(mc->size, sizeof(int));
    if(mc == NULL) {
        exit(0);
    }
}

// Função responsável por retornar a maior dimensão da imagem, para que seja possível alocar uma matriz quadrática
// obs: A abordagem da matriz quadrática foi utilizada pelo fato da varredura vertical da matriz original retornava SEGMENTATION FAULT
int checkLargerDimension(PMGImage *img) {
    int bigger;
    if(img->height < img->width) {
        bigger = img->width;
    }else
    bigger = img->height;

    return bigger;
}

// Aloca uma matriz quadrática, tendo a maior dimensão como quantidade a ser alocada nos 2 eixos
int memoryAllocatePMGImage(PMGImage *img) {
    int i;
    
    int bigger = checkLargerDimension(img);
    img->content = calloc(bigger, sizeof(int*));

    for(i = 0; i < bigger; i++){
        img->content[i] = calloc(bigger, sizeof(int));
    }

    if(img->content == NULL) {
        return ALLOCATION_ERROR;
    }

    return SUCCESS;
}

int PGMTypeCheck(PMGImage *img) {
    if(img->magicNumber[0] != 'P' || img->magicNumber[1] != '2')
        return INVALID_PGM_TYPE;
    
    return 0;
}

int memoryAllocatePMGImageAux(PMGImage *img_aux) {
    img_aux->content = malloc(sizeof(int*) * NUMBER_HEIGHT);
    for(int i=0; i<NUMBER_HEIGHT; i++){
        img_aux->content[i] = malloc(sizeof(int) * NUMBER_WIDTH);
    }

    if(img_aux->content == NULL) {
        return ALLOCATION_ERROR;
    }

    return SUCCESS;
}

// Função responsável por varrer verticalmente o conteudo da estrutura PMGImage, referente a matriz do captcha
// Assim que é identificado um elemento 1, é chamada uma função de checagem de manchas
// Se for confirmado que o elemento não é uma mancha, é chamada uma função responsável pelo isolamento daquela região da matriz original
//para uma estrutura auxiliar, a partir daí, irá ocorrer o processo de resolução do numero
// Assim que a função retornar, será incrementado o valor default da largura de uma mascara, para a varredura continuar após o numero identificado
PMGImage fetchNumber(PMGImage *img) {
    int i; //height
    int j; //width;

    for(j = 0; j < img->width; j++) {
        for(i = 0; i < img->height; i++) {
            if(img->content[i][j] == 1) {
                // printf("found at index [%d][%d]\n", i, j);
                if( !(isStartOfImage(img, i, j)) ) {
                    catchNumber(img, i, j);
                    j += NUMBER_WIDTH;
                    // printf("indice[%d][%d]\n", i, j);
                }
            }
        }
        // printf("\n");
    }
}

// Função responsável por identificar mamnchas através de uma heurística simples
int isStartOfImage(PMGImage *img, int height, int width) {
    int safe_count_h = 0;
    int safe_count_w = 0;

    for(int i = 0; i < SAFE_INTERVAL; i++) {
        if(img->content[height][width + i] == 1) safe_count_w++;
    }
    
    for(int j = 0; j < SAFE_INTERVAL; j++){
       if(img->content[height+j][width] == 1) safe_count_h++;
    }
    if(safe_count_w >= SAFE_INTERVAL-3 && 
        safe_count_h >= SAFE_INTERVAL-3) return SUCCESS;
    else return 1;

}

// Função inicializa uma estrutura auxiliar com as dimensões de uma mascara (30x50)
// Após a ĩnicialização, é copiado todos os elementos a partir do indice provindo do elemento confirmado como o inicio do numero na matriz original
// A condição de parada para essa atribuição, são as constantes, representando as dimensões de uma mascara
int catchNumber(PMGImage *img, int i_height, int j_width) {
    PMGImage img_aux; 
    newPMGImage(&img_aux);

    if(memoryAllocatePMGImageAux(&img_aux))
        return ALLOCATION_ERROR;

    for(int i = 0; i < NUMBER_HEIGHT; i++) {
        for(int j = 0; j < NUMBER_WIDTH; j++) {
            img_aux.content[i][j] = img->content[i_height+i][j_width+j];
            // printf("%d ", img_aux.content[i][j]);
        }
        // printf("\n");
    }

    //Inicio do processo de resolução do numero
    discoverNumber(&img_aux);
    // exit(1);

    //free()
    return 0;
}
// a[1] = i + 48;
void discoverNumber(PMGImage *img_aux) {
    PMGImage img_mask;
    newPMGImage(&img_mask);
    
    // char a[3];
    char *fileName = malloc(15 * sizeof(char));
    strcpy(fileName, "mascaras/x.pgm");       
    // printf("a -> %s\n", a);
    
    MASKCounters mc;
    newMASKCounter(&mc);

    int result_number = 0;
    // readImage("mascaras/3.pgm", &img_mask);
    for(int i = 0; i < 10; i++) {
        
            fileName[9] = i + 48;
            // printf("fileName -> %s\n", fileName);
            readImageMask(fileName, &img_mask);

            // printf("%s\n", fileName);
            mc.counters[i] = setCounter(img_aux, &img_mask, i); 
            // break; //BREAK PARA TESTAR APENAS UM NUMERO

            // printf("%d\n\n", mc.counters[i]);
    }
    

    // printCaptcha(img_aux);
    printf("%d - ", getLargerCounter(&mc));
}

// int checkAccuracy(PMGImage* img_aux, PMGImage *img_mask, int i) {
    

//     mc.

//     int accurracy = setCounter(img_aux, img_mask, i);

//     printf("mask %d accurracy = %d\n", i, accurracy);

//     return mc;
// }

int setCounter(PMGImage *img_aux, PMGImage *img_mask, int mask_i) {

    int counter = 0;

    for(int i = 0; i < NUMBER_HEIGHT; i++) {
        for(int j = 0; j < NUMBER_WIDTH; j++) {
            if(img_aux->content[i][j] == img_mask->content[i][j]) {
                counter++;
            }
        }
    }

    return counter;
}


// void setCounter(PMGImage *img_aux, PMGImage *img_mask, MASKCounters *mc,  int mask_i) {
//     // printPMGImage(img_aux);
//     // printf("\nACABO------------\n");
//     // printPMGImage(img_mask);
//     // printf("img_aux = %d\nimg_mask = %d\n-------\n", img_aux->content[0][0], img_mask->content[0][0] );


//     // printf("number_of_counter = %d\n\n", number_of_counter);
//     for(int i = 0; i < NUMBER_HEIGHT; i++) {
//         for(int j = 0; j < NUMBER_WIDTH; j++) {
//             // printf("img_aux->content[%d][%d] = %d\nimg_aux->content[%d][%d] = %d\n", i, j, img_aux->content[i][j], i, j, img_mask->content[i][j]);
//             if( img_aux->content[i][j] == img_mask->content[i][j]) {
//                 mc->counters[mask_i]++;
//                 // printf("i[%d][%d] = %d\n", i, j, mc->counters[mask_i]);
//             }
//         }
//     }

//     // printf("mask %d = %d\n", mask_i, mc->counters[mask_i]);
// }

int getLargerCounter(MASKCounters *mc) {
    int larger = mc->counters[0];
    int catch_index;
    for(int i = 0; i <= mc->size; i++) {
        if(larger < mc->counters[i]){
            larger = mc->counters[i];
            catch_index = i;
        }
    }

    // printf("catch_index: %d\n", catch_index);
    return catch_index;       
}

void freePMGImage(PMGImage *img) {
    for(int i = 0; i < img->height; i++) {
        free(img->content[i]);
    }

    if(img->content == NULL)
        printf("ta nulo caraio");

    free(img);
    if(img == NULL)
        printf("Ta nulo caraio");

}
