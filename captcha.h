#ifndef _captcha_
#define _captcha_

#define SUCCESS                          0
#define INVALID_NUMBER_OF_PARAMETERS    -1
#define INVALID_PGM_TYPE                -2
#define ERROR                           -3
#define INVALID_FILE                    -4
#define ALLOCATION_ERROR                 -5

#define NUMBER_HEIGHT                   50
#define NUMBER_WIDTH                    30

typedef struct captcha_ {
    char    magicNumber[2];
    int     width;
    int     height;
    int     topGrey;
    int    **content;
} PMGImage;

typedef struct maskCounters_ {
	int *counters;
	int size;
} MASKCounters;

int readImage(char*, PMGImage*);
int readImageMask(char*, PMGImage*);

void newPMGImage(PMGImage*);
void newMASKCounter(MASKCounters*);

int memoryAllocatePMGImage(PMGImage*);
int memoryAllocatePMGImageAux(PMGImage*);


int checkLargerDimension(PMGImage*);

int PGMTypeCheck(PMGImage*);

PMGImage fetchNumber(PMGImage*);
int isStartOfImage(PMGImage*, int, int);
int catchNumber(PMGImage*, int, int);

void discoverNumber(PMGImage*);
int checkAccuracy(PMGImage* img_aux, PMGImage *img_mask, int i);
int setCounter(PMGImage *img_aux, PMGImage *img_mask,  int i);
int getLargerCounter(MASKCounters*);

void printMask(PMGImage*);
void printPMGImage(PMGImage*);

void freePMGImage(PMGImage*);
#endif